#!/bin/sh


# Sync with the website hosted on Gitlab


clone_dir="/tmp/src_prepare.gitlab.io"
here_dir="$(pwd)"


clean_clone_dir() {
    [ -d "${clone_dir}" ] && rm -rfd "${clone_dir}"
}


clean_clone_dir
git clone --verbose --recursive https://gitlab.com/src_prepare/src_prepare.gitlab.io.git "${clone_dir}"
cp -r "${clone_dir}"/public/* "${here_dir}"
clean_clone_dir
git add .
git commit -m "update with pseudomirror script"
git push
