# Pages for Codeberg

This is a pseudo mirror of https://src_prepare.gitlab.io

To visit this mirror go to https://pages.codeberg.org/src_prepare/


# Why pseudo

Because it's really not a "clean" mirror. We're copying stuff from other repo and creating another one.


# Maintainers

To mirror the latest https://src_prepare.gitlab.io state clone the repo and run `bash ./pseudomirror.sh`.
A commit will be automatically created and pushed.
